var Clientes = require('../../models/dantilo');

beforeEach(()=> {Clientes.allClientes = []; });
describe('Clientes.allClientes', ()=>{
    it('Comienza vacia',()=>{
        expect(Clientes.allClientes.length).toBe(0);
    });
})

describe('Clientes.add', ()=>{
    it('agregamos un cliente', ()=>{
        expect(Clientes.allClientes.length).toBe(0);

        var a = new Clientes(1, 'blanco', 'granito', [-34.5975241,-58.386032]);
        Clientes.add(a);

        expect(Clientes.allClientes.length).toBe(1);
        expect(Clientes.allClientes[0]).toBe(a);
    });
});

describe('Clientes.findById',()=>{
    it('debe devolver el id 1',()=>{
        expect(Clientes.allClientes.length).toBe(0);
        var Cliente1= new Clientes(1, 'negro', 'madera');
        var Cliente2= new Clientes(2, 'blanco', 'granito');
        Clientes.add(Cliente1);
        Clientes.add(Cliente2);

        var targetCliente = Clientes.findById(1);
        expect(targetCliente.id).toBe(1);
        expect(targetCliente.color).toBe(Cliente1.color);
        expect(targetCliente.superficie).toBe(Cliente1.superficie);
    });
})