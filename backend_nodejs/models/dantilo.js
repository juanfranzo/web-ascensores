var Clientes = function (id, color, superficie, ubicacion){
    this.id = id;
    this.color = color;
    this.superficie = superficie;    
    this.ubicacion = ubicacion;
}

Clientes.prototype.toString = function (){
return 'id: ' + this.id + " | color: " + this.color;
}


Clientes.allClientes= [];

Clientes.add = function(cliente){
    Clientes.allClientes.push(cliente);
}

Clientes.findById= function(id){
    var aCliente = Clientes.allClientes.find(x => x.id == id);
    
    if (aCliente){
        return aCliente;
    } else {
        throw new Error(`No existe un cliente con el ID: ${id}`);
    }
}

Clientes.removeById = function(id){
    for (var i = 0; i<Clientes.allClientes.length;i++){
        if (Clientes.allClientes[i].id == id){
            Clientes.allClientes.splice(i,1);
            break;
        }
    }
}

/*
var a = new Clientes(1, 'blanco', 'granito', [-34.5975241,-58.386032]);
var b = new Clientes(2, 'rojo', 'ceramico', [-34.5848409,-58.4954466]);

Clientes.add(a);
Clientes.add(b);
*/

module.exports = Clientes;