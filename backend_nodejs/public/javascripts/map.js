// mapa de capital federal
var map = L.map('main_map').setView([-34.6170831,-58.4264813], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/* marcador personalizable
    L.marker([51.5, -0.09]).addTo(map)
    .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
    .openPopup(); */ 
    
$.ajax({
    dataType:'json',
    url:"api/dantilo2",
    success: function(result){
        console.log(result);
        result.clientela.forEach(function(cliente){
            L.marker(cliente.ubicacion, {title: cliente.id}).addTo(map);
        });
    }

})