var Clientes = require('../models/dantilo');

exports.clientes_list = function (req, res){
    res.render('clientes/index', {clientela: Clientes.allClientes});
}

exports.cliente_create_get= function (req, res){
res.render('clientes/create');
}

exports.cliente_create_post = function (req, res){
    var ubicacion = [req.body.lat, req.body.lng];    
    var cliente = new Clientes(req.body.id,
        req.body.color,
        req.body.superficie,
        ubicacion);
    Clientes.add(cliente);
    res.redirect('/dantilo1');
}

exports.cliente_update_get= function (req, res){
    var cliente = Clientes.findById(req.params.id);


    res.render('clientes/update', {cliente});
    }
    
exports.cliente_update_post = function (req, res){
        
        var cliente = Clientes.findById(req.params.id);
        cliente.id=req.body.id;
        cliente.color=req.body.color;
        cliente.superficie=req.body.superficie
        cliente.ubicacion=[req.body.lat, req.body.lng]
        
        res.redirect('/dantilo1');
    }

exports.cliente_delete_post= function(req, res){
    Clientes.removeById(req.body.id);

    res.redirect('/dantilo1')
}