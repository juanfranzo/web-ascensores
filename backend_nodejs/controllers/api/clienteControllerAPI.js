var Clientes = require('../../models/dantilo');

exports.clientes_list = function(req, res){
    res.status(200).json({
        clientela: Clientes.allClientes
    });
}

exports.cliente_create = function(req, res){
    var cliente = new Clientes (req.body.id, req.body.color, req.body.superficie)
    cliente.ubicacion=[req.body.lat, req.body.lng];
    
    Clientes.add(cliente);
    res.status(200).json({
       dantilo:cliente 
    });
}

exports.cliente_delete= function(req, res){
    Clientes.removeById(req.body.id)
    res.status(204).send();
}