var express = require('express');
var router = express.Router();
var clienteController = require('../controllers/dantilo');

router.get('/', clienteController.clientes_list);
router.get('/create', clienteController.cliente_create_get);
router.post('/create', clienteController.cliente_create_post);
router.get('/:id/update', clienteController.cliente_update_get);
router.post('/:id/update', clienteController.cliente_update_post);
router.post('/:id/delete', clienteController.cliente_delete_post);


module.exports = router;