var express = require('express');
var router = express.Router();
var clienteController = require('../../controllers/api/clienteControllerAPI');

router.get('/', clienteController.clientes_list);
router.post('/create', clienteController.cliente_create);
router.delete('/delete', clienteController.cliente_delete);


module.exports= router;